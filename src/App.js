import React from 'react';
import './App.css';

import ProductList from './components/ProductList';
import MyProvider from './components/context/MyProvider';

class App extends React.Component {
  render() {
    return (
      
        <div className="App">
            <h1 className="App-title">Almacenamiento de datos</h1>
            <MyProvider>
              <ProductList/>
            </MyProvider>
        </div>
      
    );
  };
}

export default App;
