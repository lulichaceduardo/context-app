import React, { Component } from 'react';
import Cars from './Cars'
class ProductList extends Component {
    render() {
        return (
            <div>
                <h2>Lista de productos</h2>
                {/* Pass props twice */}
                <Cars/>
                {/* <Cars
                    cars={this.props.cars}
                    incrementCarPrice={this.props.incrementCarPrice}
                    decrementCarPrice={this.props.decrementCarPrice}
                /> */}
            </div>
        );
    }
}

export default ProductList;