import React, { Component } from 'react';

class Car extends Component {
    render() {
        return (
            <div className="item">
                <p>Nombre: {this.props.name}</p>
                <p>Precio: ${this.props.price}</p>
                <button onClick={this.props.incrementPrice}>&uarr;</button>
                <button onClick={this.props.decrementPrice}>&darr;</button>
            </div>
        );
    }
}

export default Car;