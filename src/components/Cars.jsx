import React, { Component } from 'react';
import Car from './Car'
import MyContext from './context/MyContext'
class Cars extends Component {
    render() {
        return (
            <MyContext.Consumer>
                {
                    context => (
                        <div>
                            {Object.keys(context.cars).map(carID => (
                                <Car
                                    key={carID}
                                    name={context.cars[carID].name}
                                    price={context.cars[carID].price}
                                    incrementPrice={() => context.incrementCarPrice(carID)}
                                    decrementPrice={() => context.decrementCarPrice(carID)}
                                />
                            ))}
                        </div>
                    )
                }
            </MyContext.Consumer>
            
        );
    }
}

export default Cars;